.PHONY: all

ZIP = TreballFinaldeMaster.zip
LATEX = \
	main.tex \
	*/	\

all:
	mv ~/Downloads/${ZIP} .
	rm -rf ${LATEX}
	unzip -o ${ZIP}
	rm ${ZIP}
