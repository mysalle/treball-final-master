\section{Configurar el \textit{filter}}

De la mateixa manera que pel
\textit{input},
el \textit{filter} té
molts filtres,
com podem veure a la cita
\parencite{logstashFilterPlugins}.

\subsection{Dates}
Si obtenim directament les dades
dels \textit{inpunts}
anteriors d'una base de dades
generada pel Moodle,
la columna \texttt{timecreated}
la llegeix com un enter.

En realitat, el \texttt{timecreated}
no és un enter, sinó
que és un
``Unix Timestamp''.
On el zero equival
a l'u de gener del 1970
i el valor donat es la diferència
amb segons d'aquesta data.
\\

Aleshores necessitem poder
convertir aquesta data
perquè la interpreti
de forma correcta.
Per a aquesta funcionalitat
tenim el \textit{plugin}
\texttt{Date filter plugin}.

Per fer la transformació
només serà necessari
saber el format,
en el nostre cas
``Unix Timestamp'',
i en quina columna es
troba la dada
(en el nostre cas
\texttt{timecreated}).
\\

Un cop tenim tota aquesta
informació
només cal utilitzar el \textit{plugin},
on justament té facilitat
pel format
``Unix Timestamp''.
Així doncs, ens quedarà
de la forma
mostrada en l'exemple
\ref{code:filterDateLogstash}.

\lstinputlisting[
firstline=16,lastline=18,
caption={\textit{filter}
de ``Unix Timestamp'' a
una data
},
label=code:filterDateLogstash,
float,
language=logstash]
{125-manual/resources/1-config/join.conf}

\subsection{\textit{Parsejar} un \texorpdfstring{\acrshort{json}}{JSON}}

Resulta ser que una columna
de la taula de \textit{logs}
és \textit{other}.
Aquesta conté un \textit{string}
en format d'un \acrshort{json},
tal com diu la
cita \parencite{moodleJsonOther}
facilitada pel nostre client.
\\

Logstash té un \textit{plugin}
per transformar els camps
o la totalitat d'un \acrshort{json}.
Aquest és el ``JSON filter plugin''.
És tan senzill d'utilitzar
que només cal dir-li el camp que ha de \textit{parsejar}
i el cap destí.

Cal tenir en compte que
no podem fer coincidir el nom de
l'entrada amb el de la sortida.
\\

És necessari considerar que el \textit{plugin}
està prou ben dissenyat,
creant un \textit{tag}
en fallar.
Per defecte és \texttt{\_jsonparsefailure},
però pot ser personalitzat
amb un \textit{array} amb la
clau \texttt{tag\_on\_failure}.
\\

Així doncs, una possible forma
de \textit{parsejar}
la columna \texttt{other}
utilitzant el \textit{plugin}
``JSON filter plugin''
és l'exemple que veiem
a la referència
\ref{code:filterJsonLogstash}.

\lstinputlisting[
firstline=19,lastline=22,
caption={\textit{filter}
que \textit{parseja} un JSON
},
label=code:filterJsonLogstash,
float,
language=logstash]
{125-manual/resources/1-config/join.conf}

\subsection{Enriquir les dades}
Una proposta del nostre client
va ser enriquir les dades
per tal de traduir les
columnes \texttt{userid}
de la taula
\texttt{mdl\_logstore\_standard\_log}
amb els noms relacionats
per exemple.
\\

Una forma d'enriquir
les dades que ens arriben
és fent servir
el \textit{plugin}
``Translate filter plugin''.
\\

Aquest \textit{plugin}
és capaç de buscar valors
i enriquir-los amb dades.
Per fer-ho,
necessitem saber com definir
els valors,
si preferim utilitzar
\acrshort{yaml} o
\acrshort{json}.

Nosaltres opinem que amb
el \acrshort{yaml}
és suficient per a relacions directes
entre valors,
com podem veure a l'exemple
\ref{code:pluginTranslateYamlLogstash}.
\\

\begin{lstlisting}[
label=code:pluginTranslateYamlLogstash,
caption={Enriquir amb YAML},
language=yaml
]
"100": "Continue"
"200": "Ok"
"42": "answer"
"404": "Not found"
\end{lstlisting}

En canvi,
quan hi ha més d'una dada,
creiem que el \acrshort{json}
és la millor opció,
com podem veure a l'exemple
\ref{code:pluginTranslateJsonLogstash}.
\\

\lstinputlisting[
label=code:pluginTranslateJsonLogstash,
caption={Enriquir amb JSON},
language=json
]
{125-manual/resources/11-hobbits.json}

Ara que ja sabem com
han de ser els documents
és important puntualitzar
que el \textit{plugin}
sabrà si és un format o
de l'altre gràcies a l'extensió
del fitxer.
Això fa que si volem codificar
en \acrshort{yaml} l'extensió
haurà de ser \texttt{.yaml}
i si volem treballar
amb \acrshort{json}
haurà de ser \texttt{.json}.
\\

Finalment, hem de saber
configurar-lo.
Haurem de tenir en compte tres claus
que són imprescindibles
per a poder treballar.

El \texttt{source}
se'l definirà amb el
nom de la clau
on es faran les comprovacions.

El \texttt{target}
se'l definirà amb
quina clau es vol guardar.
És important tenir en compte
que no pot coincidir amb el mateix
valor que el \texttt{source}.

El \texttt{dictionary\_path}
se'l definirà amb el \textit{path}
absolut d'on es troba el document.
\\

Una vegada ja coneixem les
coses més bàsiques
podem aprofundir
amb la cita
\parencite{logstashFilterTranslate}.

Doncs ara toca descobrir
com hem de configurar la
\textit{pipeline}
perquè pugui aplicar aquest
\textit{plugin}.
Un exemple és el de la
referència
\ref{code:pluginTranslateExampleLogstahs}.
\\

\begin{lstlisting}[
label=code:pluginTranslateExampleLogstahs,
caption={configuració del Translate},
float,
language=logstash
]
filter {
  translate {
    source => "userid"
    target => "user"
    dictionary_path => "/usr/share/logstash/resources/names.json"
    fallback => "No el tenim registrat"
  }
}
\end{lstlisting}

En l'exemple \ref{code:pluginTranslateExampleLogstahs},
apreciem que hem afegit una clau
que no havíem explicat.
Aquesta és \texttt{fallback},
que només donarà el valor donat
(un \textit{string})
si no ha pogut traduir
el valor d'entrada.
És una forma d'agilitzar
la detecció d'elements
que no estiguin
afegits al nostre
fitxer.
