\section{Configurar el \textit{input}}

Logstash és una tecnologia molt
versàtil gràcies a la gran
quantitat de \textit{plugins} que conté.

Com podem veure a la
cita \parencite{logstashInputPlugins},
Logstash té una gran quantitat
de \textit{plugins}.
Alguns dels \textit{plugins}
funcionen amb Beats,
altres amb \acrshort{http} o
amb fitxers.

\subsection{MySQL i/o MariaDB}
En el nostre cas,
volem llegir les dades
d'una base de dades,
tant si és MariaDB com
si és MySQL.
\\

Primer de tot,
recomanem saber exactament
on es troba el connector
\texttt{mysql-connector-java}.

Si s'ha utilitzat
literalment la configuració
facilitada en el
\textit{Dockerfile}
\ref{code:dockerfileconnector},
llavors podem afirmar
que aquest es troba exactament
a \texttt{/usr/share/logstash/mysql-connector-java-\$\{CONNECTOR\_VERSION\}.jar}.
\\

\lstinputlisting[
caption={\textit{input} amb
connector a MySQL
\protect\footnotemark
},
label=code:connectorMySQL,
float,
language=logstash]
{125-manual/resources/5-input-connector.yml}

\footnotetext{
  Exemple de la cita
  \parencite{logstashInputMySQL}
}

Així doncs, com podem apreciar
de l'exemple mostrat
a la configuració
\ref{code:connectorMySQL},
hi ha deferents configuracions
possibles.

Un valor dels més importants
ve a ser el
\texttt{statement},
que és la crida pròpia
per obtenir les dades.
\\

Si eliminem o
comentem la línia de
\texttt{schedule}
una vegada acabada l'operació,
tancarà la \textit{pipeline}.
És rellevant considerar-ho
en el cas de que vulguem saber
quan ha acabat de
transformar totes les dades.

\subsection{Primera aproximació}

Una primera aproximació
és extreure les dades
taula per taula.
S'ha de tenir en compte
que seran taules independents
i que
Elasticsearch
no està dissenyat per fer \textit{joins}.
\\

El nostre client ens ha fet
saber quins valors són
els que per a ell són d'interès.
\\

Els valors mostrats a la referència
\ref{code:SQLusers}
són els que el nostre client
diu que li interesen
respecte la taula d'usuaris.
\\

\lstinputlisting[
caption={Dades a extreure d'usuaris
},
label=code:SQLusers,
float,
language=SQL]
{125-manual/resources/6-user.sql}

Tot seguit, la taula dels
cursos ens exposa
quins valors són els
més rellevants,
mostrats a la referència
\ref{code:SQLcourses}.
\\

\lstinputlisting[
caption={Dades a extreure dels
cursos
},
label=code:SQLcourses,
float,
language=SQL]
{125-manual/resources/7-course.sql}

I finalment la taula dels
\textit{logs} exposa
quins són els valors
interessants mostrats a la
referència
\ref{code:SQLlogs}.
\\

\lstinputlisting[
caption={Dades a extreure dels
\textit{logs}
},
label=code:SQLlogs,
float,
language=SQL]
{125-manual/resources/8-logs.sql}

Llavors, una primera
aproximació per llegir les
dades de la base de
dades amb
Logstash
pot ser fer el
que veiem a la
referència
\ref{code:logstashInputExemple}.

\lstinputlisting[
caption={
Extreure dades d'un
MySQL
amb
Logstash
},
label=code:logstashInputExemple,
float,
language=logstash]
{125-manual/resources/9-exemple-input-course.logstash}

L'exemple de la referència
\ref{code:logstashInputExemple}
és destacable.
En primer lloc, perquè
podem apreciar l'ús
de variables
per poder ajustar-nos
a les necessitats de cada
moment.
Per exemple,
tant l'usuari de
la base de dades com
la contrasenya
estan parametritzades
amb les variables
d'entorn
\texttt{DB\_USER} i
\texttt{DB\_PASSWORD}
respectivament.

En segon lloc, perquè
veiem un ús real
per extreure les dades,
on és important donar
el \textit{path}
complet d'on es troba
el connector de MySQL
per a Java a la clau
\texttt{jdbc\_driver\_library}.

En tercer lloc, perquè
és un exemple real
d'utilització
del Logstash per
extreure dades
d'un MySQL, on
les dades han estat
generades per un
Moodle.
Això ens permet
veure els paràmetres
mínims per poder fer
l'extracció de dades.

I finalment, perquè
no ens sembla d'interès
afegir els altres
exemples d'altres
taules.
Això es deu a que l'únic
valor que canvia
és el \texttt{statment}
entre els diferents
\textit{inputs},
i aquests han estat
descrits
amb els exemples
\ref{code:SQLusers},
\ref{code:SQLcourses} i
\ref{code:SQLlogs}.

\subsection{Aproximació amb \textit{Joins}}

És interessant
utilitzar
els \textit{joins}
en temps d'extracció
de les dades,
perquè encara que
Elasticsearch
sigui capaç de fer-los,
no és el seu punt fort.
Elasticsearch
destaca
per la capacitat
de buscar informació, i
no pas per fer \textit{joins}.
\\

Llavors, una altra
aproximació
per extreure les dades
és fer servir
el \textit{join}
al mateix temps que el MySQL
per tal d'absorbir
totes les dades
que desitgem
amb un sol índex
d'Elasticsearch.
\\

Dins de MySQL
tenim la utilitat
\texttt{LEFT JOIN}.
Aquesta respecta
tota la taula
nomenada
prèviament i
afegeix les dades
de la nova taula
a sobre.

Aleshores,
si desitgem
unificar les tres
taules anteriors:
\texttt{mdl\_logstore\_standard\_log},
\texttt{mdl\_course} i
\texttt{mdl\_user},
la taula principal
on volem la totalitat
de la informació
és la taula
\texttt{mdl\_logstore\_standard\_log}.
\\

Coneixent totes les
dades d'interès,
una forma vàlida
per extreure
les dades
és l'exemple
que trobem en la referència
\ref{code:SQLjoin}.

\lstinputlisting[
caption={
Obtenir tots
els paràmetres
amb un \textit{join}
},
label=code:SQLjoin,
float,
language=SQL]
{125-manual/resources/10-join.sql}

\subsection{Optimitzant les crides}

Si volem evitar que cada
vegada cridi la taula
completa,
és necessari saber
des de quin punt continuar.

Per sort, el \textit{plugin}
ja té una facilitat per resoldre
aquest problema.
\\

El \textit{plugin}
defineix per defecte
el valor \texttt{sql\_last\_value}
a 0.

Llavors, si
l'utilitzem amb el valor
\texttt{timestamp},
tot allò que sigui
anterior al 1970
mai entrarà en efecte,
ja que el zero equival
a l'u de gener de 1970.

Per contra,
si coneixem el valor
\texttt{id} de les taules
al Moodle llavors
són comptadors que
comencen per l'u.
\\

Un cop tenim clar quina
columna hem d'utilitzar,
(el \texttt{id})
només necessitem
definir al \textit{plugin} com
treballar.

Al \texttt{statement}
haurem de d'utilitzar
el filtre \texttt{WHERE}
amb la facilitat que ens
proporciona el \textit{plugin}.

\begin{lstlisting}[
language=SQL,
caption={Optimitzar les crides
amb Logstash}
]
  WHERE id > :sql_last_value
\end{lstlisting}

També haurem d'habilitar
i puntualitzar quina és
la columna que ha de fer cas:

\begin{lstlisting}[
language=logstash,
caption={Configurar el filtre del
\textit{input} a Logstash}
]
  use_column_value => true
  tracking_column => "id"
\end{lstlisting}

\subsection{Conclusió}

Una manera de configurar el
\textit{input}
amb una sola \textit{pipeline}
i que aquesta
sigui incremental
la podem trobar a l'exemple mostrat
a la referència
\ref{code:inputJoinLogstash},
on aquesta està
vinculada al \texttt{join.sql}
\ref{code:inputJoinSql}.
\\

\lstinputlisting[
firstline=1,lastline=13,
caption={\textit{input}
incremental amb \textit{joins}
},
label=code:inputJoinLogstash,
float,
language=logstash]
{125-manual/resources/1-config/join.conf}

\lstinputlisting[
caption={join.sql},
label=code:inputJoinSql,
float,
language=SQL]
{125-manual/resources/1-config/join.sql}

En general
ens sembla una bona
resolució per optimitzar
les crides a base de dades.
Potser en el cas del client
no acaba de ser la millor
aposta,
ja que el nostre client
li agradaria anar eliminant
la base de dades mentre
és llegida.
Això significa que podria
eliminar els valors
no desitjats abans d'executar
el Logstash.
\\

Per altra banda,
gràcies a la flexibilitat
que proporcionen les
taules \acrshort{sql}
i a la força
que té Logstash
podem contrarestar
les limitacions
d'Elasticsearch.
