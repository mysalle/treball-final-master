\section{Configurar les \textit{pipelines}}

Dins del contenidor Docker de Logstash,
on es donen a conèixer totes les \textit{pipelines}
al procés de Logstash
és al \textit{path}
\texttt{/usr/share/logstash/config/pipelines.yml}.
\\

Així que si volem configurar més d'una \textit{pipeline}
perquè tenim més d'un valor d'entrada i/o
sortida ens interessarà configurar-los perquè
poden obtenir un resultat com el que
es veu a la figura \ref{fig:allPipelines}.
\\

\begin{figure}
    \centering
    \includegraphics{125-manual/resources/0-pipelines/pipelines.eps}
    \caption{Les \textit{pipelines} de Logstash}
    \label{fig:allPipelines}
\end{figure}

La configuració quedarà com es veu a la configuració \ref{code:pipelinesLogstash}
inspirada de la cita \parencite{multiplePipelinesLogstash}.
\lstinputlisting[caption={\textit{pipelines} de Logstash},
label=code:pipelinesLogstash,
float,
language=yaml]
{125-manual/resources/2-pipelines.yml}

A l'exemple de la configuració
\ref{code:pipelinesLogstash},
hi ha diferents paràmetres que es
poden configurar, com podem veure
a la cita \parencite{logstashYml}.

\section{Configurar \texttt{pipelines.yml}}

Tot seguit descriurem
les configuracions que ens
han semblat més rellevants:

\subsection{\texttt{pipeline.id}}
Aquest és l'identificador
de la \textit{pipeline}.
Això significa que tots els logs
que facin referència a aquesta estaran
nomenats pel valor posat.

Evitar que els noms es repeteixin
i procurar que siguin aclaridors entre
els processos ens ajudarà
a entendre què està passant
dins del Logstash.

\subsection{\texttt{pipeline.ordered}}
En cas de voler respectar l'ordre
d'entrada i sortida,
com si fos una \acrshort{lifo},
pot ser interessant posar aquest
valor a \texttt{true}.
\\

Tot i ser una configuració de valor
(en el nostre cas l'ordre no sembla rellevant),
usem valors de la
base de dades com són el
\texttt{id} i el \texttt{timestamp}.
Aquests dos valors fan
que l'ordre en que les dades són
emmagatzemades a la base de dades
sigui irrellevant.

\subsection{\texttt{path.config}}
Aquí es posa el \textit{path}
on es troba la configuració
que desitgem que sigui executada.
\\

Si utilitzem aquesta configuració
ens permetrà fer un \textit{reload}
com podem veure a la cita
\parencite{LogstashReload},
tant per línia de comandes:
\texttt{--config.reload.automatic},
com executant quan un desitja
l'event \texttt{SIGHUP},
per exemple executant la comanda
\texttt{kill -SIGHUP <pid>}.
\\

És important que, en recarregar
la configuració,
els fitxers de configuració
estiguin ben formatats.
És per aquest motiu que
es recomana activament
sobreescriure el document
reanomenant el fitxer abans
de modificar-lo directament.

\subsection{config.string}
Amb aquesta clau,
donem directament
la configuració de com
volem que funcioni la \textit{pipeline}.
\\

Per a configuracions petites i senzilles
pot estar bé,
però en crear la configuració
amb aquesta clau,
l'única forma de rectificar
la configuració
és matant el procés de Logstash
i tornar-lo a arrancar.
No té cap forma
per recarregar la configuració
en calent.

El \subsection{\texttt{config.test\_and\_exit}}
comprova que la configuració sigui
vàlida i acabada.
\\

Resulta útil usar-lo
quan no s'està del tot segur
que la configuració sigui
vàlida,
però encara no volem
utilitzar el Logstash.

\subsection{\texttt{config.reload.automatic}}
En cas de tenir planejat fer unes quantes proves
i voler modificar la configuració en calent
pot ser interessant configurar-lo així.
\\

Cal anar amb compte quan fem \texttt{reloads} automàtics,
ja que poden llegir la configuració
sense que sigui modificada del tot.
Llavors es recomana modificar el fitxer
amb instruccions atòmiques com el
\texttt{rename}
\footnote{Només pels casos
concrets que són atòmics
com veiem a la \acrshort{url}
\url{https://man7.org/linux/man-pages/man2/rename.2.html}}
abans de fer servir
una edició directament.
\\

Si s'utilitza aquest valor
és important tenir present
que, per defecte, l'interval de \texttt{checks}
per recarregar la configuració és
de 3 segons, i que pot ser modificat
amb la clau \texttt{config.reload.interval}.

\subsection{\texttt{config.debug}}
Quan passen coses que no acabem
d'entendre,
una manera de facilitar-nos la tasca poden
ser els logs.

Aquests no sempre són complets,
llavors podem fer-los més \textit{verboses}
amb aquesta clau.
\\

Tenir present que aquí mostrarà tota la
informació que llegeixi i interpreti en
clar.
Això significa que si hi ha contrasenyes
les mostraran els \textit{logs} en text pla.

\subsection{\texttt{queue.type}}
És un valor molt interessant
perquè defineix si volem que les
dades es guardin en memòria o en disc.
\\

També té una repercussió important en
el rendiment,
ja que escriure en disc
és molt més lent que escriure en
memòria.
Però en disc té l'avantatge
de que en cas d'apagat,
totes les dades que han entrat
pel \textit{input} no
es pedràn encara
que la màquina s'apagui.

\subsection{\texttt{path.logs}}
Per defecte,
dins del \textit{container} Docker de Logstash,
els \textit{logs} són mostrats per la sortida
estàndard.
Però pot ser que sigui d'utilitat
tenir els \textit{logs} en un
directori específic més que mostrar-los
d'aquesta forma.

Sobretot és interessant si
el procés que ha de llegir els logs
també és un \textit{container} Docker.

\subsection{\texttt{pipeline.separate\_logs}}
La suma de tots els \textit{logs} i
de totes les \textit{pipelines}
simultànies pot ser caòtica,
confusa i poc entenedora.
\\

Per poder resoldre els problemes
o entendre millor què
està passant en tot moment,
pot ser d'interès separar
els \textit{logs} de cada \textit{pipeline}
en documents independents.
