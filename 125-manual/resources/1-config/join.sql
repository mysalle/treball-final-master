SELECT
  mdl_logstore_standard_log.id as id,
  mdl_logstore_standard_log.eventname,
  mdl_logstore_standard_log.component,
  mdl_logstore_standard_log.action,
  mdl_logstore_standard_log.target,
  mdl_logstore_standard_log.userid,
  mdl_user.username,
  mdl_user.firstname,
  mdl_user.lastname,
  mdl_user.email,
  mdl_logstore_standard_log.courseid,
  mdl_course.fullname,
  mdl_course.shortname,
  mdl_logstore_standard_log.other,
  mdl_logstore_standard_log.timecreated
FROM mdl_logstore_standard_log
LEFT JOIN (mdl_user, mdl_course) ON (
  mdl_logstore_standard_log.userid = mdl_user.id AND
  mdl_logstore_standard_log.courseid = mdl_course.id
)
WHERE mdl_logstore_standard_log.id > :sql_last_value
